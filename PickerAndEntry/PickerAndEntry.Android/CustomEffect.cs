﻿using Android.Graphics.Drawables;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using PickerAndEntry.Android;
using Color = Android.Graphics.Color;

[assembly: ResolutionGroupName("TestEffect")]
[assembly: ExportEffect(typeof(CustomEffect), nameof(CustomEffect))]
namespace PickerAndEntry.Android
{
    public class CustomEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            var background = new GradientDrawable();
            background.SetColor(Color.Transparent);
            background.SetStroke(3, Color.Gray);
            background.SetPadding(20,20,20,20);
            Control.SetBackground(background);
        }

        protected override void OnDetached()
        {
        }
    }
}