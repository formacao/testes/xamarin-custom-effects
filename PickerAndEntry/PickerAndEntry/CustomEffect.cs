﻿using Xamarin.Forms;

namespace PickerAndEntry
{
    public class CustomEffect : RoutingEffect
    {
        public CustomEffect() : base($"TestEffect.{nameof(CustomEffect)}")
        {
        }
    }
}