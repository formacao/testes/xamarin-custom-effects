﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PickerAndEntry
{
    public partial class MainPage : ContentPage
    {
        public IEnumerable<string> Items => new List<string>
        {
        "Test 01", "Test 02", "Test 03", "Test 04", "Test 05"
        };
        public MainPage()
        {
            InitializeComponent();
            BindingContext = this;
        }
    }
}